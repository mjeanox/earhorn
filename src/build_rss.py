import json
from os import path

from feedgen.feed import FeedGenerator

# Docs: https://python-feedgen.readthedocs.io/en/v0.9.0/

DIR = path.abspath(".")
URL = "https://earhorn.us"

# Initialize feed
fg = FeedGenerator()
fg.load_extension("podcast")

# Add metadata
fg.podcast.itunes_category("Arts", "Performing Arts")
fg.title("Earhorn")
# fg.link( href=f'{URL}', rel='alternate' )
fg.link(href=f"{URL}/rss.xml", rel="self")
fg.subtitle("Poetry reading series")  # i.e. 'description'
fg.language("en")
# fg.id('http://lernfunk.de/media/654321')
# fg.author( {'name':'John Doe','email':'john@example.de'} )
# fg.logo('http://ex.com/logo.jpg')

# Add entry
# fg.contributor( name='John Doe', email='jdoe@example.com' )
fe = fg.add_entry()
fe.id(f"{URL}/media/beenie-man.mp3")
fe.title("The First Episode")
fe.description("Enjoy our first episode.")
fe.enclosure(f"{URL}/media/beenie-man.mp3", 0, "audio/mpeg")

# Generate feed
rssfeed = fg.rss_str(pretty=True)  # Get the RSS feed as string

# Write out
print(f"Writing RSS to {path.join(DIR, 'public', 'rss.xml')}")
fg.rss_file(path.join(DIR, "public", "rss.xml"))  # Write the RSS feed to a file
fg.rss_file(path.join(DIR, "public", "rss.xml"))  # Write the RSS feed to a file
