Podcast Feed
============

TODO
----

*   Read Apple spec carefully and add what is required.
    Will have to get some dummy artwork.
*   Update feed and retry the validation on Podcasts Connect

Podcast docs
------------

`Apple Podcasts Connect <https://podcastsconnect.apple.com/>`__

`Feegen docs <https://python-feedgen.readthedocs.io/en/v0.9.0/>`__

Apple podcast specifications:

*   `Validate howto <https://itunespartner.apple.com/podcasts/articles/validate-your-podcast-3064>`__
*   `Technical requirements <https://itunespartner.apple.com/podcasts/articles/podcast-requirements-3058>`__
*   `Required tags <https://help.apple.com/itc/podcasts_connect/#/itcb54353390>`__
*   `Creating an Apple podcast <https://itunespartner.apple.com/podcasts/>`__

Testing
-------

Host locally for testing: ``python -m SimpleHTTPServer 8000``

